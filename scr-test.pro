TEMPLATE = app
TARGET = rotation

QT += widgets sensors

INCLUDEPATH += .

# Input
HEADERS += screen-test.h
SOURCES += screen-test.cpp

CONFIG += silent
