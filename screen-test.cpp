/*
	*
	* This file is a part of CoreStuff.
	* An activity viewer for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "screen-test.h"

ScreenTest::ScreenTest() : QDialog() {

	orientation = new QLabel();
	orientation->setAlignment( Qt::AlignCenter );

	mainScreen = qApp->primaryScreen();
	connect( mainScreen, SIGNAL( primaryOrientationChanged( Qt::ScreenOrientation ) ), this, SLOT( changeRotation( Qt::ScreenOrientation ) ) );

	sensor = new QOrientationSensor( this );
	connect( sensor, SIGNAL( readingChanged() ), this, SLOT( changeRotation() ) );

	changeRotation( Qt::PrimaryOrientation );

	QVBoxLayout *lyt = new QVBoxLayout();
	lyt->addStretch();
	lyt->addWidget( orientation, Qt::AlignCenter );
	lyt->addStretch();

	setLayout( lyt );
};

void ScreenTest::changeRotation() {

	QOrientationReading *reading = sensor->reading();
	QSize size = mainScreen->size();

	qDebug() << "Orientation sensor";

	switch( reading->orientation() ) {
		case QOrientationReading::TopUp: {

			if ( size.width() > size.height() )
				orientation->setText( "Landscape mode" );

			else
				orientation->setText( "Portrait mode" );

			break;
		}

		case QOrientationReading::TopDown: {

			if ( size.width() > size.height() )
				orientation->setText( "Inverted Landscape mode" );

			else
				orientation->setText( "Inverted Portrait mode" );

			break;
		}

		case QOrientationReading::LeftUp:
		case QOrientationReading::RightUp: {

			if ( size.width() > size.height() )
				orientation->setText( "Portrait mode" );

			else
				orientation->setText( "Landscape mode" );

			break;
		}

		case QOrientationReading::FaceUp: {

			break;
		}

		case QOrientationReading::FaceDown: {

			break;
		}

		case QOrientationReading::Undefined: {

			break;
		}
	}
};

void ScreenTest::changeRotation( Qt::ScreenOrientation scrOrient ) {

	qDebug() << "QScreen";

	switch( scrOrient ) {
		case Qt::PrimaryOrientation: {

			QSize size = mainScreen->size();
			if ( size.width() > size.height() )
				orientation->setText( "Landscape mode" );

			else
				orientation->setText( "Portrait mode" );

			break;
		}

		case Qt::LandscapeOrientation: {

			orientation->setText( "Landscape mode" );
			break;
		}

		case Qt::PortraitOrientation: {

			orientation->setText( "Portrait mode" );
			break;
		}

		case Qt::InvertedLandscapeOrientation: {

			orientation->setText( "Inverted Landscape mode" );
			break;
		}

		case Qt::InvertedPortraitOrientation: {

			orientation->setText( "Inverted Portrait mode" );
			break;
		}
	}
};

int main( int argc, char *argv[] ) {

	QApplication app( argc, argv );

	ScreenTest *UI = new ScreenTest();

	return UI->exec();
};
